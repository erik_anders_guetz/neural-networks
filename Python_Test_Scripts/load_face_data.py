# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 00:26:50 2018

@author: Erik Guetz
"""

import os
import numpy as np
import time

from PIL import Image
import imageio

from skimage.feature import hog
from skimage import data, exposure

import matplotlib.pyplot as plt

import png


start_time = time.time()

face_dir = r"D:\GIT_REPOS\NeuralNetworks\Face_Images\Clay"
new_face_dir = r"D:\GIT_REPOS\NeuralNetworks\new_face"
cifar_dir = r"D:\GIT_REPOS\NeuralNetworks\cifar"

for f in os.listdir(face_dir):
    
    if f.endswith('.jpg'):
        
        os.remove(os.path.join(face_dir, f))
    
#    if f.endswith('.jpg'):
#        
#        with Image.open(os.path.join(face_dir, f)) as im:
#            
#            im = im.resize((128,128))
#            
#            im.save(os.path.join(face_dir, f.replace('.jpg', '')+'.png'), 'png')

#for f in os.listdir(face_dir):
#    
#    if f.endswith('.png'):
#        
#        image = imageio.imread(os.path.join(face_dir, f))
#        
#        red = image[:, :, 0]
#        green = image[:, :, 1]
#        blue = image[:, :, 2]
#        
#        image = np.squeeze((0.299 * red) + (0.587 * green) + (0.114 * blue))
#        
#        fd, hog_image = hog(image, orientations=8, pixels_per_cell=(4, 4),
#                    cells_per_block=(1, 1), block_norm='L2-Hys', visualise=True)
#        
#        hog_image_rescale = np.array(255 * exposure.rescale_intensity(hog_image, in_range=(0, 10))).astype('uint8', order='F')
#        
#        with open(os.path.join(face_dir, f), 'wb') as outfile:
#            
#            png.from_array((hog_image_rescale), mode='L').save(outfile)
#            
#import pickle
#import png
#
#fpath = os.path.join(r'D:\GIT_REPOS\NeuralNetworks\cifar-10-batches-py', 'data_batch_1')
#
#f = open(fpath, 'rb')
#
#d = pickle.load(f, encoding='bytes')
## decode utf8
#d_decoded = {}
#for k, v in d.items():
#    d_decoded[k.decode('utf8')] = v
#
#    d = d_decoded
#    f.close()
#
#for i, filename in enumerate(d['filenames']):
#    
#    q = d['data'][i]
#    
#    filename = 'train_%i' % i
#    
#    with open(os.path.join(cifar_dir, filename + '.png'), 'wb') as outfile:
#        png.from_array(q.reshape((32, 32, 3), order='F').swapaxes(0,1), mode='RGB').save(outfile)

#for f in os.listdir(face_dir):
#    
#    train_data = np.zeros((1518, 128, 128))
#    train_labels = np.ones((1518, 1))
#    
#    count = 0
#    
#    if f.endswith('.png'):
#        
#        with open(os.path.join(face_dir, f), 'rb') as fopen:
#            
#            fopen.readline()
#            
#            height = int(fopen.readline())
#            width = int(fopen.readline())
#            
#            depth = int(fopen.readline())
#            
#            assert depth <= 255
#            
#            raster = []
#            
#            for y in range(height):
#                row = []
#                for y in range(width):
#                    row.append(ord(fopen.read(1)))
#                raster.append(row)
#                
#            raster = np.array(raster)
#            
#        train_data[count, :, :] = raster
#        
#        count += 1
#        
#end_time = time.time()
#
#print("Execution time: ", (end_time-start_time))
#
#np.save('train_data.npy', train_data)
#np.save('train_labels.npy', train_labels)