# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 19:47:22 2018

@author: Erik Guetz
"""
import sys

sys.path.append(r"D:\GIT_REPOS\NeuralNetworks\Python_Test_Scripts\facematch")
sys.path.append(r"D:\GIT_REPOS\NeuralNetworks\Python_Test_Scripts\facematch\align")

import matplotlib.pyplot as plt
import numpy as np

import facial_recognition as fr
import face_detect as fd
import cv2

from PIL import Image
#
#from skimage.feature import hog
#from skimage import data, exposure

image = cv2.imread(r"D:\GIT_REPOS\NeuralNetworks\Face_Images\Other\picamera_image_0.jpg")

correct_faces = fd.get_output(image)

red = image[:, :, 0]
green = image[:, :, 1]
blue = image[:, :, 2]

image = np.squeeze((0.299 * red) + (0.587 * green) + (0.114 * blue))

for face in correct_faces:
    
    new_image = image[face[1]:face[1]+256, face[0]:face[0]+256]
    
    new_image = Image.fromarray(new_image)
    new_image = new_image.resize((128, 128))
    new_image = np.array(new_image)
    
    print(fr.get_output([new_image]))
    
    plt.imshow(new_image, cmap=plt.cm.gray)
    plt.pause(0.01)