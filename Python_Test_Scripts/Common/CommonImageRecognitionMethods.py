# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 17:04:57 2018

@author: Erik Guetz
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 12:59:56 2018

@author: Erik Guetz
"""

import keras

from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation, Dropout
from keras.layers import Conv2D, MaxPooling2D

import os

class ImageRecognitionModel():
    
    def __init__(self):
        
        self.batch_size = 32
        self.generations = 100
        self.prediction_count = 20
        
        self.save_directory = 'D:\\GIT_REPOS\\NeuralNetworks\\saved_keras_models\\'
        
    def init_model(self):
        
        self.model = Sequential()
        
        self.model.add(Conv2D(32, (3, 3), padding='same', input_shape=self.x_train.shape[1:]))
        self.model.add(Activation('relu'))
        
        self.model.add(Conv2D(32, (3, 3)))
        self.model.add(Activation('relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        
        self.model.add(Conv2D(64, (3, 3), padding='same'))
        self.model.add(Activation('relu'))
        
        self.model.add(Conv2D(64, (3, 3)))
        self.model.add(Activation('relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        
        self.model.add(Conv2D(128, (3, 3), padding='same'))
        self.model.add(Activation('relu'))
        
        self.model.add(Conv2D(128, (3, 3)))
        self.model.add(Activation('relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        
        self.model.add(Flatten())
        
        self.model.add(Dense(512))
        self.model.add(Activation('relu'))
        
        self.model.add(Dropout(0.5))
        
        self.model.add(Dense(self.classes))
        self.model.add(Activation('softmax'))
        
    def compile_model(self):
        
        self.optimizers = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
        
        self.model.compile(loss='categorical_crossentropy', optimizer=self.optimizers, metrics=['accuracy'])
        
    def train_model(self):
        
        self.x_train = self.x_train.astype('float32')
        self.x_test = self.x_test.astype('float32')
        
        self.x_train /= 255
        self.x_test /= 255
        
        self.model.fit(self.x_train, self.y_train, batch_size=self.batch_size, \
        epochs=self.generations, validation_data=(self.x_test, self.y_test))
    
    def save_model(self):
        
        if not os.path.isdir(self.save_directory):
            
            os.mkdir(self.save_directory)
            
        print(self.save_directory)
            
        self.model_path = os.path.join(self.save_directory, self.model_name)
        
        self.model.save(self.model_path)
        
        print('Saved the trained model to %s' % self.model_path)
        
    def evaluate_model(self):
        
        self.score = self.model.evaluate(self.x_test, self.y_test, verbose=1)
        
        print('Test Loss:', self.score[0])
        print('Test Accuracy:', self.score[1])