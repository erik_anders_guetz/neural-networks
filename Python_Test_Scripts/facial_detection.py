#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      guetze
#
# Created:     02/03/2018
# Copyright:   (c) guetze 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import numpy as np
import tensorflow as tf
import os

import glob
import random

import matplotlib.image as mpimg
import matplotlib.pyplot as plt

# Initialize training and evaluation data

random.seed(123)

parent_dir = r"D:\GIT_REPOS\NeuralNetworks"

use_path = 'train_images'
train_paths = glob.glob(os.path.join(parent_dir, use_path)+'/**/*.png', recursive=True)
train_labels = [int(s.split('\\')[-2] == 'positive') for s in train_paths]
tmp = list(zip(train_paths, train_labels))
random.shuffle(tmp)
train_paths, train_labels = zip(*tmp)

use_path = 'validation_images'
validation_paths = glob.glob(os.path.join(parent_dir, use_path)+'/**/*.png', recursive=True)
validation_labels = [int(s.split('\\')[-2] == 'positive') for s in validation_paths]
tmp = list(zip(validation_paths, validation_labels))
random.shuffle(tmp)
validation_paths, validation_labels = zip(*tmp)

use_path = 'test_images'
test_paths = glob.glob(os.path.join(parent_dir, use_path)+'/**/*.png', recursive=True)
test_labels = [int(s.split('\\')[-2] == 'positive') for s in test_paths ]
tmp = list(zip(test_paths, test_labels))
random.shuffle(tmp)
test_paths, test_labels = zip(*tmp)

data_g1 = tf.Graph()

with data_g1.as_default():
    
    tf_train_paths = tf.constant(train_paths)
    tf_train_labels = tf.constant(train_labels)
    tf_valid_paths = tf.constant(validation_paths)
    tf_valid_labels = tf.constant(validation_labels)
    tf_test_paths = tf.constant(test_paths)
    tf_test_labels = tf.constant(test_labels)
    
    train_dataset = tf.data.Dataset.from_tensor_slices((tf_train_paths,
                                                        tf_train_labels)) 
    valid_dataset = tf.data.Dataset.from_tensor_slices((tf_valid_paths,
                                                        tf_valid_labels)) 
    test_dataset = tf.data.Dataset.from_tensor_slices((tf_test_paths,
                                                       tf_test_labels)) 
    
    iterator = tf.data.Iterator.from_structure(train_dataset.output_types,
                                               train_dataset.output_shapes)
    
    next_element = iterator.get_next()
    
    train_iter_init = iterator.make_initializer(train_dataset)
    valid_iter_init = iterator.make_initializer(valid_dataset)
    test_iter_init = iterator.make_initializer(test_dataset)
    
#with tf.Session(graph=data_g1) as sess:
#
#    sess.run(train_iter_init)
#    for i in range(3):
#        print('Fetch element #%d from training dataset:' % (i+1))
#        ele = sess.run(next_element)
#        print(ele)
#    
#    print()
#    sess.run(valid_iter_init)
#    for i in range(3):
#        print('Fetch element #%d from validation dataset:' % (i+1))
#        ele = sess.run(next_element)
#        print(ele)
#        
#    print()
#    sess.run(test_iter_init)
#    for i in range(3):
#        print('Fetch element #%d from test dataset:' % (i+1))
#        ele = sess.run(next_element)
#        print(ele)
        
def read_image_jpg_onehot(path, label):
    str_tensor = tf.read_file(path)
    decoded_image = tf.image.decode_jpeg(str_tensor, channels=1, fancy_upscaling=False)

    decoded_image = tf.cast(decoded_image, tf.float32)
    decoded_image = decoded_image / 255.

    onehot_label = tf.one_hot(label, depth=2)
    return decoded_image, onehot_label

BATCH_SIZE = 128

def datareader():
    tf_train_paths = tf.constant(train_paths)
    tf_train_labels = tf.constant(train_labels)
    tf_valid_paths = tf.constant(validation_paths)
    tf_valid_labels = tf.constant(validation_labels)
    
    train_dataset = tf.data.Dataset.from_tensor_slices((tf_train_paths,
                                                        tf_train_labels)) 
    valid_dataset = tf.data.Dataset.from_tensor_slices((tf_valid_paths,
                                                        tf_valid_labels)) 
    
    ############################################################
    ## Custom data transformation; 
    #  here: image reading, shuffling, batching
    train_dataset = train_dataset.map(read_image_jpg_onehot,
                                      num_parallel_calls=4)
    train_dataset = train_dataset.shuffle(buffer_size=1000)
    train_dataset = train_dataset.batch(BATCH_SIZE)
    
    valid_dataset = valid_dataset.map(read_image_jpg_onehot,
                                      num_parallel_calls=4)
    valid_dataset = valid_dataset.batch(BATCH_SIZE)
    ############################################################

    iterator = tf.data.Iterator.from_structure(train_dataset.output_types,
                                               train_dataset.output_shapes)

    next_element = iterator.get_next(name='next_element')
    
    train_iter_init = iterator.make_initializer(train_dataset,name='train_iter_init')
    valid_iter_init = iterator.make_initializer(valid_dataset,name='valid_iter_init')
    
    return next_element

#data_g2 = tf.Graph()
#with data_g2.as_default():
#    datareader()
#    
#with tf.Session(graph=data_g2) as sess:
#
#    sess.run('train_iter_init')
#    for i in range(3):
#        print('Fetch batch #%d from training dataset:' % (i+1))
#        images, labels = sess.run(['next_element:0', 'next_element:1'])
#        print(images.shape, labels.shape)
#        
#    print()
#    sess.run('valid_iter_init')
#    for i in range(3):
#        print('Fetch batch #%d from validation dataset:' % (i+1))
#        images, labels = sess.run(['next_element:0', 'next_element:1'])
#        print(images.shape, labels.shape)
        
##########################
### SETTINGS
##########################

# Hyperparameters
n_epochs = 40
n_iter = n_epochs * (len(train_paths) // BATCH_SIZE)
        
g = tf.Graph()
with g.as_default():
    
    tf.set_random_seed(123)
    
    learning_rate = tf.placeholder(tf.float32, shape=[])

    # Input data
    next_element = datareader()
    
    tf_images = tf.placeholder_with_default(next_element[0],
                                            shape=[None, 32, 32, 1], 
                                            name='images')
    tf_labels = tf.placeholder_with_default(next_element[1], 
                                            shape=[None, 2], 
                                            name='labels')
    
    tf_images = tf.cast(tf_images, dtype=tf.float32)
    
    conv1 = tf.layers.conv2d(inputs=tf_images, filters=32, kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    conv2 = tf.layers.conv2d(inputs=pool1, filters=64, kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

    pool2_flat = tf.reshape(pool2, [-1, 8 * 8 * 64])
    
    dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
    dropout = tf.layers.dropout(inputs=dense, rate=0.1, training=True)
    out_layer = tf.layers.dense(inputs=dropout, units=2)

    # Loss and optimizer
    loss = tf.nn.softmax_cross_entropy_with_logits(logits=out_layer, labels=tf_labels)
    cost = tf.reduce_mean(loss, name='cost')
    optimizer = tf.train.AdamOptimizer()
    train = optimizer.minimize(loss=cost, name='train', global_step=tf.train.get_global_step())

    # Prediction
    prediction = tf.argmax(out_layer, 1, name='prediction')
    correct_prediction = tf.equal(tf.argmax(tf_labels, 1), tf.argmax(out_layer, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')
    
    
def lr(epochs):
    
    return 0.01 * np.exp(epochs/30)
    
    
def train_network(graph):
    with tf.Session(graph=graph) as sess:
        sess.run('train_iter_init')
        sess.run(tf.global_variables_initializer())
        saver0 = tf.train.Saver(tf.global_variables(), reshape=True)
        
        avg_cost = 0.
        iter_per_epoch = n_iter // n_epochs
        epoch = 0
    
        for i in range(n_iter):
            
            _, cost = sess.run(['train', 'cost:0'])
            avg_cost += cost
            if not i % iter_per_epoch:
                epoch += 1
                avg_cost /= iter_per_epoch
                print("Epoch: %03d | AvgCost: %.5f" % (epoch, avg_cost))
                avg_cost = 0.
                sess.run('train_iter_init')
        
        saver0.save(sess, save_path='./face_detect')
    
img_paths = np.array(test_paths)
labels = np.array(test_labels)

def validate_network(img_paths, labels):
    
    saver1 = tf.train.import_meta_graph('./face_detect.meta')
    
    with tf.Session() as sess:
        
        saver1.restore(sess, save_path='./face_detect')
        
        num_correct = 0
        cnt = 0
        for path, lab in zip(img_paths, labels):
            cnt += 1
            image = mpimg.imread(path)
            
            if image.shape == (32, 32, 3):
                image = 0.299*image[:, :, 0] + 0.587*image[:, :, 1] + 0.114*image[:, :, 2]
            
            image = image.reshape(1, 32, 32, 1)
            
            pred = sess.run('prediction:0', 
                             feed_dict={'images:0': image})
    
            num_correct += int(lab == pred[0])
            #print('Label: ' + str(lab) + ' prediction: ' + str(pred))
            if cnt % 30 == 0:
                plt.imshow(np.squeeze(image), cmap=plt.cm.gray)
                plt.title('no: '+ str(cnt) + ' Label: ' + str(lab) + ' prediction: ' +str(pred))
                plt.pause(0.1)
            
        acc = num_correct / cnt * 100
        
    print('Test Accuracy: %0.1f%%' % acc)
    
def get_output(images):
    
    saver1 = tf.train.import_meta_graph('./face_detect.meta')
    
    with tf.Session() as sess:
        
        saver1.restore(sess, save_path='./face_detect')
        
        correct_indexes = []
        count = 0
        
        for image in images:
        
            image = image.reshape(1, 32, 32, 1)
            
            pred =  sess.run('prediction:0', feed_dict = {'images:0': image})
            
            if pred == 1:
                
                correct_indexes.append(count)
                
            count += 1
            
        return correct_indexes
    
train_network(g)
validate_network(img_paths, labels)