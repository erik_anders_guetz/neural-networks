# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 12:36:23 2018

@author: Erik Guetz
"""

import sys

sys.path.append("./Libraries/")

from NumpyNeuralNetwork import Sequential, Layer
import random

num_points = 20000

slope = random.random()
intercept = 0

input_data = []

expected_data = []

for i in range(num_points):
    
    x = random.random()
    y = random.random()
    
    height = ((slope * x) + intercept)

    if y > height:

        expected_data.append(1)
        
    else:
        
        expected_data.append(0)
        
    input_data.append([x, y])
    
model = Sequential()
        
model.addLayer(Layer(2))
model.addLayer(Layer(1))

model.train(input_data, expected_data, epochs=30, lr=0.01)
model.evaluate(input_data, expected_data)