#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      guetze
#
# Created:     02/03/2018
# Copyright:   (c) guetze 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import numpy as np
import tensorflow as tf
import math
import time

# Initialize training and evaluation data

train_labels = np.asarray(np.load(r'training_labels.npy'))
train_data = np.load(r'training_data.npy')
eval_data = np.load(r'eval_data.npy')
eval_labels = np.asarray(np.load(r'eval_labels.npy'))

#------------------------------------------------------------------------------

# Initialize Network Parameters

learning_rate = 0.1
batch_size = 100
num_steps = math.floor(len(train_data)/batch_size)
disp_step = math.floor(num_steps/10)

hidden_num_1 = 100
hidden_num_2 = 100
input_num = 10000
output_num = 2

# Initialize Tensorflow placeholders

X = tf.placeholder('float', [None, input_num])
Y = tf.placeholder('float', [None, output_num])

# Initialize network structure

weights = {
    'h1': tf.Variable(tf.random_normal([input_num, hidden_num_1])),
    'h2': tf.Variable(tf.random_normal([hidden_num_1, hidden_num_2])),
    'out': tf.Variable(tf.random_normal([hidden_num_2, output_num]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([hidden_num_1])),
    'b2': tf.Variable(tf.random_normal([hidden_num_2])),
    'out': tf.Variable(tf.random_normal([output_num]))
}

# Initialize network feedforward structure

def network(x):
    
    l1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    l2 = tf.add(tf.matmul(l1, weights['h2']), biases['b2'])
    out = tf.add(tf.matmul(l2, weights['out']), biases['out'])
    
    return out

logits = network(X)
prediction = tf.nn.softmax(logits)

# Define Loss and Optimizer Functions

loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

# Define measurement tools

correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Initialize Tensorflow backend variables

init = tf.global_variables_initializer()

start_time = time.time()

with tf.Session() as sess:
    
    sess.run(init)
    #writer = tf.summary.FileWriter("logs", sess.graph)
    
    for step in range(0, num_steps-1):
        
        batch_x, batch_y = train_data[batch_size*step:batch_size*(step+1)], train_labels[batch_size*step:batch_size*(step+1)]
        
        sess.run(train_op, feed_dict={X:batch_x, Y:batch_y})
        
        if step % disp_step == 0:
            pass
            loss, acc = sess.run([loss_op, accuracy], feed_dict={X:batch_x, Y:batch_y})
            print("Step %s, Minibatch loss= %f, Training Accuracy= %f" % (str(step), loss, acc))
            
    print("Optimization Finished!")
    end_time = time.time()
    print("Testing Accuracy: ", end="")
    print(sess.run(accuracy, feed_dict={X:eval_data, Y:eval_labels})*100)
            
    
print("Train Time: ", (end_time-start_time))