# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 13:01:10 2018

@author: guetze
"""

import random
import numpy as np

def add_random_circle(random_map):
    
    radius = 12.5
    offset_x = 0.5 * random.random() + 0.25
    offset_y = 0.5 * random.random() + 0.25
    
    new_random_map = np.zeros((100, 100, 1))
    
    for y in range(100):
        
        for x in range(100):
            
            if (x-offset_x)**2 + (y-offset_y)**2 <= radius**2:
                
                new_random_map[y][x] = random.random()
                
    return random_map + new_random_map

def generate_train_data(epochs):
    
    train_data = np.zeros((epochs, 100, 100, 1))
    train_labels = np.zeros((epochs, 2))
    
    for i in range(0, epochs, 2):
        
        random_map = np.random.random((100, 100, 1))
        train_data[i] = random_map
        train_labels[i] = [1, 0]
        
        train_data[i+1] = add_random_circle(random_map)
        train_labels[i+1] = [0, 1]
        
    train_data = np.float32(train_data).reshape(epochs, 10000)
    train_labels = np.int32(train_labels).reshape(epochs, 2)
        
    return train_data, train_labels

td, tl = generate_train_data(8000)
np.save(r'training_data.npy', td)
np.save(r'training_labels.npy', tl)
ed, el = generate_train_data(2000)
np.save(r'eval_data.npy', ed)
np.save(r'eval_labels.npy', el)