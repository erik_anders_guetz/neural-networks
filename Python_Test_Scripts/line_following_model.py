# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 11:49:17 2018

@author: Erik Guetz
"""

import numpy as np
import matplotlib.pyplot as plt
import random

def _sigmoid(z):
    
    return (1/(1 + np.exp(-z)))

def _sigmoid_deriv(z):
    
    return _sigmoid(z) * (1 - _sigmoid(z))

## Initialize Random Line
    
rand_slope = random.random()
rand_int = 0#random.random()

print('Y = %0.2fx + %0.2f' % (rand_slope, rand_int))

## Generate Random Points and classify them

points = []

for i in range(10000):
    
    rand_x = random.random()
    rand_y = random.random()
    
    if rand_y > ((rand_slope * rand_x) + rand_int):
        
        points.append(([rand_x, rand_y], [1, 0]))
        
    else:
        
        points.append(([rand_x, rand_y], [0, 1]))
        
epochs = 1

w1 = 2 * np.random.random((2, 3)) - 1
w2 = 2 * np.random.random((3, 3)) - 1
w3 = 2 * np.random.random((3, 1)) - 1
    
for j in range(1):
    
    inputs = np.array([1, 1]).reshape(1, 2)
    
    expected = 0
    
    l1 = _sigmoid(np.dot(inputs, w1))
    l2 = _sigmoid(np.dot(l1, w2))
    l3 = _sigmoid(np.dot(l2, w3))
    
    error_l3 = expected - l3

    print('E3', error_l3)
    
    delta_l3 = error_l3 * _sigmoid_deriv(l3)

    print('D3', delta_l3)
    
    error_l2 = np.dot(delta_l3, w3.T)

    print('E2', error_l2, error_l2.shape)
    
    delta_l2 = error_l2 * _sigmoid_deriv(l2)

    print('D2', delta_l2, delta_l2.shape)
    
    error_l1 = np.dot(delta_l3, w3.T)

    print('E1', error_l1, error_l1.shape)
    
    delta_l1 = error_l1 * _sigmoid_deriv(l1)

    print('D1', delta_l1, delta_l1.shape)
    
    w1 += np.dot(inputs.T, delta_l1)
    w2 += np.dot(l1.T, delta_l2)
    w3 += np.dot(l2.T, delta_l3)
    
count = 0
correct_count = 0
fin = []

for j in range(8000, 10000):
    
    count += 1
    
    expected = points[j][1]
    inputs = points[j][0]
    
    t = _sigmoid(np.dot(inputs, w1))
    t1 = _sigmoid(np.dot(t, w2))
    output = _sigmoid(np.dot(t1, w3))
    
    output = [int(round(i)) for i in output]
    
    if output == expected:
        
        correct_count += 1
    
    if output == [1, 0]:
        
        fin.append((inputs[0], inputs[1], 'rx'))
    
    elif output == [0, 1]:
    
        fin.append((inputs[0], inputs[1], 'bx'))
    

    
print('Accuracy: ', (correct_count/count))

for i in range(len(fin)):
    
    plt.plot(fin[i][0], fin[i][1], fin[i][2])
    
x = np.linspace(0, 1)

line = rand_slope * x + rand_int;

plt.plot(x, line, 'g')

plt.show()
