#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      guetze
#
# Created:     02/03/2018
# Copyright:   (c) guetze 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import numpy as np
import tensorflow as tf
import os

import glob
import random

import matplotlib.image as mpimg
import matplotlib.pyplot as plt

# Initialize training and evaluation data

random.seed(123)

parent_dir = r"D:\GIT_REPOS\NeuralNetworks"

use_path = 'Face_Images'
train_paths = glob.glob(os.path.join(parent_dir, use_path)+'/**/*.png', recursive=True)
train_labels = []
for s in train_paths:
    if s.split('\\')[-2] == "Caleb":
        train_labels.append(1)
    elif s.split('\\')[-2] == "Erik":
        train_labels.append(2)
    elif s.split('\\')[-2] == "Clay":
        train_labels.append(3)
    else:
        train_labels.append(0)
        
tmp = list(zip(train_paths, train_labels))
random.shuffle(tmp)
train_paths, train_labels = zip(*tmp)

data_g1 = tf.Graph()

with data_g1.as_default():
    
    tf_train_paths = tf.constant(train_paths)
    tf_train_labels = tf.constant(train_labels)

    train_dataset = tf.data.Dataset.from_tensor_slices((tf_train_paths,
                                                        tf_train_labels)) 
    
    iterator = tf.data.Iterator.from_structure(train_dataset.output_types,
                                               train_dataset.output_shapes)
    
    next_element = iterator.get_next()
    
    train_iter_init = iterator.make_initializer(train_dataset)
    
#with tf.Session(graph=data_g1) as sess:
#
#    sess.run(train_iter_init)
#    for i in range(3):
#        print('Fetch element #%d from training dataset:' % (i+1))
#        ele = sess.run(next_element)
#        print(ele)
        
def read_image_jpg_onehot(path, label):
    str_tensor = tf.read_file(path)
    decoded_image = tf.image.decode_jpeg(str_tensor, channels=1, fancy_upscaling=False)

    decoded_image = tf.cast(decoded_image, tf.float32)
    decoded_image = decoded_image / 255.

    onehot_label = tf.one_hot(label, depth=4)
    return decoded_image, onehot_label

BATCH_SIZE = 25

def datareader():
    tf_train_paths = tf.constant(train_paths)
    tf_train_labels = tf.constant(train_labels)
    
    train_dataset = tf.data.Dataset.from_tensor_slices((tf_train_paths,
                                                        tf_train_labels)) 
    
    ############################################################
    ## Custom data transformation; 
    #  here: image reading, shuffling, batching
    train_dataset = train_dataset.map(read_image_jpg_onehot,
                                      num_parallel_calls=4)
    train_dataset = train_dataset.shuffle(buffer_size=1000)
    train_dataset = train_dataset.batch(BATCH_SIZE)
    
    ############################################################

    iterator = tf.data.Iterator.from_structure(train_dataset.output_types,
                                               train_dataset.output_shapes)

    next_element = iterator.get_next(name='next_element')
    
    train_iter_init = iterator.make_initializer(train_dataset,name='train_iter_init')
    
    return next_element

#data_g2 = tf.Graph()
#with data_g2.as_default():
#    datareader()
#    
#with tf.Session(graph=data_g2) as sess:
#
#    sess.run('train_iter_init')
#    for i in range(3):
#        print('Fetch batch #%d from training dataset:' % (i+1))
#        images, labels = sess.run(['next_element:0', 'next_element:1'])
#        print(images.shape, labels.shape)
        
##########################
### SETTINGS
##########################

# Hyperparameters
n_epochs = 12
n_iter = n_epochs * (len(train_paths) // BATCH_SIZE)
        
g = tf.Graph()
with g.as_default():
    
    tf.set_random_seed(123)
    
    learning_rate = tf.placeholder(tf.float32, shape=[])

    # Input data
    next_element = datareader()
    
    tf_images = tf.placeholder_with_default(next_element[0],
                                            shape=[None, 128, 128, 1], 
                                            name='images')
    tf_labels = tf.placeholder_with_default(next_element[1], 
                                            shape=[None, 4], 
                                            name='labels')
    
    tf_images = tf.cast(tf_images, dtype=tf.float32)
    
    conv1 = tf.layers.conv2d(inputs=tf_images, filters=32, kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    conv2 = tf.layers.conv2d(inputs=pool1, filters=64, kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

    pool2_flat = tf.reshape(pool2, [-1, 32 * 32 * 64])
    
    dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
    dropout = tf.layers.dropout(inputs=dense, rate=0.6, training=True)
    out_layer = tf.layers.dense(inputs=dropout, units=4)

    # Loss and optimizer
    loss = tf.nn.softmax_cross_entropy_with_logits_v2(logits=out_layer, labels=tf_labels)
    cost = tf.reduce_mean(loss, name='cost')
    optimizer = tf.train.AdamOptimizer()
    train = optimizer.minimize(loss=cost, name='train', global_step=tf.train.get_global_step())

    # Prediction
    prediction = tf.argmax(out_layer, 1, name='prediction')
    correct_prediction = tf.equal(tf.argmax(tf_labels, 1), tf.argmax(out_layer, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')
    
    
def lr(epochs):
    
    return 0.01 * np.exp(epochs/30)
    
    
def train_network(graph):
    with tf.Session(graph=graph) as sess:
        sess.run('train_iter_init')
        sess.run(tf.global_variables_initializer())
        saver0 = tf.train.Saver(tf.global_variables(), reshape=True)
        
        avg_cost = 0.
        iter_per_epoch = n_iter // n_epochs
        epoch = 0
    
        for i in range(n_iter):
            
            _, cost = sess.run(['train', 'cost:0'])
            avg_cost += cost
            if not i % iter_per_epoch:
                epoch += 1
                avg_cost /= iter_per_epoch
                print("Epoch: %03d | AvgCost: %.5f" % (epoch, avg_cost))
                avg_cost = 0.
                sess.run('train_iter_init')
        
        saver0.save(sess, save_path='./face_recog')
    
img_paths_recog = np.array(train_paths)
labels_recog = np.array(train_labels)

def validate_network(img_paths, labels):
    
    saver2 = tf.train.import_meta_graph('./face_recog.meta')
    
    with tf.Session() as sess:
        
        saver2.restore(sess, save_path='./face_recog')
        
        num_correct = 0
        cnt = 0
        for path, lab in zip(img_paths, labels):
            cnt += 1
            image = mpimg.imread(path)
            
            if image.shape == (128, 128, 3):
                image = 0.299*image[:, :, 0] + 0.587*image[:, :, 1] + 0.114*image[:, :, 2]
            
            image = image.reshape(1, 128, 128, 1)
            
            pred = sess.run('prediction:0', 
                             feed_dict={'images:0': image})
    
            num_correct += int(lab == pred[0])
            #print('Label: ' + str(lab) + ' prediction: ' + str(pred))
            if cnt % 30 == 0:
                plt.imshow(np.squeeze(image), cmap=plt.cm.gray);
                plt.title('no: '+ str(cnt) + ' Label: ' + str(lab) + ' prediction: ' +str(pred))
                plt.pause(0.1)
            
        acc = num_correct / cnt * 100
        
    print('Test Accuracy: %0.1f%%' % acc)
    
def get_output(images):
    
    saver2 = tf.train.import_meta_graph('./face_recog.meta')
    
    with tf.Session() as sess:
        
        saver2.restore(sess, save_path='./face_recog')
        
        correct_indexes = []
        count = 0
        
        for image in images:
            
            image = image.reshape(1, 128, 128, 1)
            
            pred =  sess.run('prediction:0', feed_dict = {'images:0': image})
            
            print(pred)
                
            correct_indexes.append(pred)
            
        return correct_indexes
    
#train_network(g)
#validate_network(img_paths_recog, labels_recog)