# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 19:31:36 2018

@author: Erik Guetz
"""

import sys, os
import imageio
import numpy as np

import time

sys.path.append('D:\\GIT_REPOS\\NeuralNetowrks\\Python_Test_Scripts\\Libraries\\')

from NumpyNeuralNetwork import Layer, Sequential

data_dir = "D:\\GIT_REPOS\\NeuralNetowrks\\DevanagariDataset\\"

if not(os.path.isfile('./training_data.npy')):

    start_time = time.clock()
    
    training_data = np.zeros((78200, 1, 1024))
    expected_data = np.zeros((78200, 1, 46))
    
    file_count = 0
    character_count = 0
    
    for root, dirs, files in os.walk(data_dir):
        
        root_split = root.split('\\')
        
        if root_split[-1] == "Train":
            
            for roots, dirs, files in os.walk(root):
                
                roots_split = roots.split('\\')
                character = roots_split[-1].split("_")[-1]
                
                for file in os.listdir(roots):
                    
                    if character != "Train":
                        ## Convert .png to numpy array
                
                        filename = os.path.join(roots, file)
                        
                        image = imageio.imread(filename)
                        image = np.array(image)
                        training_data[file_count, :, :] = image.flatten()
                        
                        new_base = np.zeros((1, 46))
                        new_base[0, character_count-1] = 1
                        
                        expected_data[file_count, :, :] = new_base
                        
                        file_count += 1
                        
                character_count += 1
                
    end_time = time.clock()
    
    np.save('training_data', training_data)
    np.save('expected_data', expected_data)
    
    print('Load time: %0.2f seconds' % (end_time-start_time))
    
else:
    
    training_data = np.load('training_data.npy')
    expected_data = np.load('expected_data.npy')

model = Sequential()

model.addLayer(Layer(1024))
model.addLayer(Layer(64))
model.addLayer(Layer(46))

model.train(training_data, expected_data, epochs=1)
model.evaluate(training_data, expected_data)