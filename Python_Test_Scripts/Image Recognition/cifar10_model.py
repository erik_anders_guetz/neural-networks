# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 12:59:56 2018

@author: Erik Guetz
"""

import sys

sys.path.append('D:\\GIT_REPOS\\NeuralNetworks\\Python_Test_Scripts\\Common\\')

import CommonImageRecognitionMethods

import keras
from keras.datasets import cifar10

class Cifar10Model(CommonImageRecognitionMethods.ImageRecognitionModel):
    
    def __init__(self):
        
        super(Cifar10Model, self).__init__()
        
        self.classes = 10
        self.generations = 10
        
        self.model_name = 'cifar10_model.h5'
        
        (self.x_train, self.y_train), (self.x_test, self.y_test) = cifar10.load_data()
        
        print('x_train shape:', self.x_train.shape)
        print(self.x_train.shape[0], 'training samples')
        print(self.x_test.shape[0], 'testing samples')
        
        self.y_train = keras.utils.to_categorical(self.y_train, self.classes)
        self.y_test = keras.utils.to_categorical(self.y_test, self.classes)
        
        self.init_model()
        
        print(self.model.summary())
        
        self.compile_model()
        self.train_model()
        self.save_model()
        self.evaluate_model()
        
    def compile_model(self):
        
        self.optimizers = keras.optimizers.Adadelta()
        
        self.model.compile(loss='categorical_crossentropy', optimizer=self.optimizers, metrics=['accuracy'])
        
nn = Cifar10Model()