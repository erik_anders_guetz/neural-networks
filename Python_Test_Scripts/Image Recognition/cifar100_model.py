# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 16:57:32 2018

@author: Erik Guetz
"""

import sys
import scipy.io

sys.path.append('D:\\Desktop\\Bradley\\Junior Year\\Neural Net\\Code\\Common\\')

import CommonImageRecognitionMethods

import keras
from keras.datasets import cifar100

from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation, Dropout
from keras.layers import Conv2D, MaxPooling2D

class Cifar100Model(CommonImageRecognitionMethods.ImageRecognitionModel):
    
    def __init__(self):
        
        super(Cifar100Model, self).__init__()
        
        self.classes = 100
        
        self.model_name = 'cifar100_model.h5'
        (self.x_train, self.y_train), (self.x_test, self.y_test) = cifar100.load_data(label_mode='fine')
        
        print('x_train shape:', self.x_train.shape)
        print(self.x_train.shape[0], 'training samples')
        print(self.x_test.shape[0], 'testing samples')
        
        scipy.io.savemat('D:\\Desktop\\Bradley\\Junior Year\\Neural Net\\Code\\data.mat', mdict={'data': self.x_train[0]})
        
        self.y_train = keras.utils.to_categorical(self.y_train, self.classes)
        self.y_test = keras.utils.to_categorical(self.y_test, self.classes)
        
        self.init_model()
        
        print(self.model.summary())
        
nn = Cifar100Model()