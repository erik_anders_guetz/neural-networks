# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 16:57:32 2018

@author: Erik Guetz
"""

import sys

sys.path.append('D:\\Desktop\\Bradley\\Junior Year\\Neural Net\\Code\\Common\\')

import CommonImageRecognitionMethods

import keras
from keras.datasets import mnist

from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation, Dropout
from keras.layers import Conv2D, MaxPooling2D

class HandwritingModel(CommonImageRecognitionMethods.ImageRecognitionModel):
    
    def __init__(self):
        
        super(HandwritingModel, self).__init__()
        
        self.batch_size = 64
        self.classes = 10
        self.generations = 10
        
        self.model_name = 'handwriting_model.h5'
        
        (self.x_train, self.y_train), (self.x_test, self.y_test) = mnist.load_data()
        
        print('x_train shape:', self.x_train.shape)
        print(self.x_train.shape[0], 'training samples')
        print(self.x_test.shape[0], 'testing samples')
        
        self.y_train = keras.utils.to_categorical(self.y_train, self.classes)
        self.y_test = keras.utils.to_categorical(self.y_test, self.classes)
        
        self.init_model()
        
        print(self.model.summary())
        
        self.compile_model()
        self.train_model()
        self.save_model()
        self.evaluate_model()
        
    def init_model(self):
        
        img_rows, img_cols = 28, 28
        
        self.x_train = self.x_train.reshape(self.x_train.shape[0], img_rows, img_cols, 1)
        self.x_test = self.x_test.reshape(self.x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)
        
        self.model = Sequential()
        
        self.model.add(Conv2D(32, (3, 3), input_shape=input_shape))
        self.model.add(Activation('relu'))
        
        self.model.add(Conv2D(32, (3, 3)))
        self.model.add(Activation('relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        
        self.model.add(Conv2D(64, (3, 3), padding='same'))
        self.model.add(Activation('relu'))
        
        self.model.add(Conv2D(64, (3, 3)))
        self.model.add(Activation('relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.5))
        
        self.model.add(Conv2D(128, (3, 3), padding='same'))
        self.model.add(Activation('relu'))
        
        self.model.add(Conv2D(128, (3, 3)))
        self.model.add(Activation('relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        
        self.model.add(Flatten())
        
        self.model.add(Dense(512))
        self.model.add(Activation('relu'))
        
        self.model.add(Dropout(0.5))
        
        self.model.add(Dense(self.classes))
        self.model.add(Activation('softmax'))
    
    def compile_model(self):
        
        self.optimizer = keras.optimizers.Adadelta()
        
        self.model.compile(loss='categorical_crossentropy', optimizer=self.optimizer, metrics=['accuracy'])
        
nn = HandwritingModel()