import tensorflow as tf
from align import detect_face
import cv2
import numpy as np

import sys
sys.path.append(r"D:\GIT_REPOS\NeuralNetworks\facematch\align")

minsize = 20
threshold = [0.6, 0.7, 0.7]
factor = 0.709
margin = 44
input_image_size = 160

sess = tf.Session()
pnet, rnet, onet = detect_face.create_mtcnn(sess, 'align')

def get_output(img):
    faces = []
    face_rects = []
    img_size = np.asarray(img.shape)[0:2]
    bounding_boxes, _ = detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
    if not len(bounding_boxes) == 0:
        for face in bounding_boxes:
            if face[4] > 0.50:
                det = np.squeeze(face[0:4])
                bb = np.zeros(4, dtype=np.int32)
                bb[0] = np.maximum(det[0] - margin / 2, 0)
                bb[1] = np.maximum(det[1] - margin / 2, 0)
                bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
                bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
                cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
                resized = cv2.resize(cropped, (input_image_size,input_image_size),interpolation=cv2.INTER_AREA)
                faces.append({'face':resized,'rect':[bb[0],bb[1],bb[2],bb[3]]})
    
    for face in faces:
        center = [int((face['rect'][0] + face['rect'][2])/2), int((face['rect'][1] + face['rect'][3])/2)]
        new_rect = [0, 0]
        if center[0] - 128 >= 0:
            new_rect[0] = center[0]-128
        else:
            new_rect[0] = 0
        if center[1] - 128 >= 0:
            new_rect[1] = center[1]-128
        else:
            new_rect[1] = 0
        face_rects.append(new_rect)
    
    return face_rects

#img = cv2.imread(r"D:\GIT_REPOS\NeuralNetworks\Face_Images\Other\picamera_image_0.jpg")
#
#print(get_output(img))