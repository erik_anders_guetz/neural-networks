import numpy as np
import time

class Sequential():

    def __init__(self):
    
        self.model = []
        self.epochs = 0
        self.lr = 0

    def addLayer(self, layer_obj):

        if len(self.model) < 1:

            self.model.append(layer_obj)

        else:

            self.model.append(Weights(self.model[-1].size, layer_obj.size))
            self.model.append(layer_obj)

    def train(self, input_data, exp_output, epochs=10001, lr=0.1):

        print('Training Model ... ', end="")
        start_time = time.time()
        
        self.epochs = epochs
        self.lr = lr

        for ep in range(epochs):
            
            for n in range(len(input_data)):

                try:
                    
                    self.model[0].layer = np.array(input_data[n]).reshape(1, len(input_data[n]))

                except:
					
                    self.model[0].layer = np.array(input_data[n])

                for i in range(2, len(self.model), 2):

                    self.model[i].feedForward(self.model[i-1], self.model[i-2])

                output = self.model[-1].layer
                
                error = output - exp_output[n]
                
                for i in range(len(self.model)-1, 0, -2):
                
                    delta = error * self.model[i].activation_deriv()
                    
                    self.model[i-1].weights -= lr * np.dot(np.array(self.model[i-2].layer).T, delta)
                
                    error = np.dot(delta, np.array(self.model[i-1].weights).T)

        end_time = time.time()
        
        print('Done!')
        print('Training Time: %0.3f seconds' % (end_time - start_time))
        print('Average time per Epoch: %0.3f seconds' % ((end_time - start_time)/self.epochs))

    def test(self, input_data):
            
        self.model[0].layer = input_data
        
        for i in range(2, len(self.model), 2):
        
            self.model[i].feedForward(self.model[i-1], self.model[i-2])
            
            output = self.model[-1].layer
        
        return output

    def evaluate(self, input_data, expected_data):
    
        count = 0
        
        for i in range(len(input_data)):
        
            output = np.argmax(self.test(input_data[i])[0])
            
            base = np.zeros((1, 46))
            base[:, output] = 1
            
            print(base.shape, expected_data[i].shape)
            print(base, expected_data[i])
            
            if base == expected_data[i]:
            
                count += 1
        
        print('Accuracy: %0.2f percent' % (100 * (float(count)/(len(input_data)))))


class Weights():

    def __init__(self, input_size, output_size):
    
        self.input_size = input_size
        self.output_size = output_size
        
        self.weights = []
        
        self.init_weights()
    
    def init_weights(self):
            
        self.weights = 2 * np.random.random((self.input_size, self.output_size)) - 1

class Layer():

    def __init__(self, size, activation='sigmoid'):
    
        self.size = size
        self.activation = activation
        
        self.layer = []
        
        self.init_layer()
    
    def init_layer(self):
    
        self.layer = np.zeros(self.size)
    
    def sigmoid(self, z):
    
        return 1 / (1 + np.exp(-z))
    
    def sigmoid_deriv(self):
    
        return self.sigmoid(self.layer) * (1 - self.sigmoid(self.layer))
    
    def relu(self, z):
        
        return (z*[z>0])[0]
    
    def relu_deriv(self):
        
        out = [(self.layer)>0]
        return [x*1 for x in out][0]
    
    def activation_deriv(self):
        
        if self.activation == 'sigmoid':
    
            return self.sigmoid_deriv()
        
        elif self.activation == 'relu':
            
            return self.relu_deriv()

    def feedForward(self, weights_obj, layer_obj):
        
        if self.activation == 'sigmoid':
    
            self.layer = self.sigmoid(np.dot(layer_obj.layer, weights_obj.weights))
        
        elif self.activation == 'relu':
            
            self.layer = self.relu(np.dot(layer_obj.layer, weights_obj.weights))