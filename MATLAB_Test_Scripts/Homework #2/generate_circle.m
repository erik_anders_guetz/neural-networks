function [ out ] = generate_circle( xdim, ydim, radius, offset_x, offset_y)

    % Generates a two dimensional image of a circle of size xdim * ydim

    [X, Y] = meshgrid(1:xdim, 1:ydim);
        
    out = (X-offset_x).^2 + (Y-offset_y).^2 <= radius^2;
      
end

