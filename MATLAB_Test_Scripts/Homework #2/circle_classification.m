% Name : circle_classification
% Author : Guetz, Erik
% Function: Creates and classifies between 6 separately colored circles

clc
clear all
close all

%% Initialize the circle parameters

xdim = 29;
ydim = 29;
offset_x = 15;
offset_y = 15;
radius = 7;

%% Initialize Neural Network Parameters

input_dimension = xdim*ydim*3;
noise_coeff = 1;
epochs = 1001;
hidden_neurons = 6;
bias_size = 1;
learning_rate = 0.1;

%% Initialize Default Circles

[rcv, gcv, bcv, pcv, ycv, ccv] = generate_training_data(xdim, ydim, radius, offset_x, offset_y);

training_set = [rcv; gcv; bcv; pcv; ycv; ccv];
expected_set = [1 0 0; 0 1 0; 0 0 1; 1 0 1; 1 1 0; 0 1 1];

%% Initialize Random Weights

w0 = 2 * rand(input_dimension, hidden_neurons) - 1;
w1 = 2 * rand(hidden_neurons, 3) - 1;

%% Training Red Circle
disp('Training');
[num_inputs, ~] = size(training_set);
tic();
for n = 1:epochs
    
    for i = 1:num_inputs

        l0 = training_set(i, :) + noise_coeff * rand(size(training_set(i, :))) - 0.5 * noise_coeff; % Set the input neurons
        expected = expected_set(i, :); % Expected output

        l1 = feedForward(w0, l0);
        l2 = feedForward(w1, l1); % Calculate the hypothesized output

        error_l2 = l2 - expected; % Calculate the output error

        delta_l2 = error_l2 .* relu_deriv(l2); % Use the sigmoid derivative 
                                               % to calculate the layer delta.

        error_l1 = delta_l2 * w1'; % Calculate the previous layer error given 
                                   % the delta from above and current layers weights

        delta_l1 = error_l1 .* relu_deriv(l1); % Use the sigmoid derivative 
                                               % to calculate the layer delta.

        w1 = w1 - learning_rate * (l1' * delta_l2); % Fix the weights based on the layer delta
        w0 = w0 - learning_rate * (l0' * delta_l1);
    
    end
    
end
toc();

%% Evaluating Network
noise_coeff = 2;
disp('Evaluating')
tic();
correct_count = 0;

for n = 1:epochs
    
    for i = 1:num_inputs

        l0 = training_set(i, :) + noise_coeff * rand(size(training_set(i, :))) - 0.5 * noise_coeff;
        expected = expected_set(i, :);
        % Calculate the hypothesis
        output = round(feedForward(w1, feedForward(w0, l0)));
        % Count the number of correct hypotheses 
        if output == expected

            correct_count = correct_count + 1;

        end

    end

end
toc();
% Print how accurate the evaluated neural network is
fprintf('Accuracy: %0.2f percent\n', (correct_count/(num_inputs*epochs))*100)