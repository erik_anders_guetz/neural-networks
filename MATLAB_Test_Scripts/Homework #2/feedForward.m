function [ out ] = feedForward( weights, layer )

    % Returns the forward propogated output of the activation function given
    % the previous layer and the current layers weights.
    out = sigmoid(layer * weights);

end

