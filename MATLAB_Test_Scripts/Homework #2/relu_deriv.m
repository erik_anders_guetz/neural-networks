function [ out ] = relu_deriv( x )

       % Returns the derivative of the Recrified Linear Unit Function.
       % If x > 0, returns 1. Else returns 0.

    out = x;

    [m, n] = size(x);
    alpha = 0;

    for i = 1:m

        for j = 1:n

            out(i, j) = x(i, j) > 0;

        end

    end

end