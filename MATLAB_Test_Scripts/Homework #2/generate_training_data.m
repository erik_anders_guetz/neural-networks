function [ rcv, gcv, bcv, pcv, ycv, ccv] = generate_training_data(xdim, ydim, radius, offset_x, offset_y)

    % This function generates a red, green, blue, purple, yellow, and cyan 
    % circle and then returns its vector.

    circle = generate_circle(xdim, ydim, radius, offset_x, offset_y);

    red_circle = zeros(xdim, ydim, 3);
    green_circle = zeros(xdim, ydim, 3);
    blue_circle = zeros(xdim, ydim, 3);
    purple_circle = zeros(xdim, ydim, 3);
    yellow_circle = zeros(xdim, ydim, 3);
    cyan_circle = zeros(xdim, ydim, 3);

    red_circle(:, :, 1) = circle;
    green_circle(:, :, 2) = circle;
    blue_circle(:, :, 3) = circle;
    purple_circle(:, :, 1) = circle;
    purple_circle(:, :, 3) = circle;
    yellow_circle(:, :, 1) = circle;
    yellow_circle(:, :, 2) = circle;
    cyan_circle(:, :, 2) = circle;
    cyan_circle(:, :, 3) = circle;

    rcv = red_circle(:)';
    gcv = green_circle(:)';
    bcv = blue_circle(:)';
    pcv = purple_circle(:)';
    ycv = yellow_circle(:)';
    ccv = cyan_circle(:)';

end

