function out = sigmoid_deriv( x )

    % Returns the derivative of the sigmoid function at point x.

    out = sigmoid(x) .* (1 - sigmoid(x));

end

