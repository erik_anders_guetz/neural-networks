function out = sigmoid( z )

    % Returns the value of the sigmoid function at point z.

    out = 1./(1 + exp(-z));

end

