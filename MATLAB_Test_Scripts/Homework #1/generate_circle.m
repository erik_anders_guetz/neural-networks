function [ out ] = generate_circle( xdim, ydim, radius, offset_x, offset_y, color)

    [X, Y] = meshgrid(1:xdim, 1:ydim);
    
    if color == 1
        
        out = (X-offset_x).^2 + (Y-offset_y).^2 <= radius^2;
       
    elseif color == 0
        
        out = ~(((X-offset_x).^2 + (Y-offset_y).^2 <= radius^2));
        
    end

end

