clc
clear all
close all

xdim = 11;
ydim = 11;
offset_x = 6;
offset_y = 6;
radius = 3;
epochs = 10001;
hidden_neurons = 30;

%% Initialize Default Circles

circle = generate_circle(xdim, ydim, radius, offset_x, offset_y, 1);
test_circle = generate_circle(xdim, ydim, radius, offset_x, offset_y, 0);

red_circle = zeros(xdim, ydim, 3);
red_circle(:, :, 1) = circle;

green_circle = zeros(xdim, ydim, 3);
green_circle(:, :, 2) = circle;

blue_circle = zeros(xdim, ydim, 3);
blue_circle(:, :, 3) = circle;

red_circle_vect = red_circle(:)';
green_circle_vect = green_circle(:)';
blue_circle_vect = blue_circle(:)';

%% Initialize Random Weights

w0 = 2 * rand(xdim*ydim*3, hidden_neurons) - 1;
w1 = 2 * rand(hidden_neurons, 3) - 1;

%% Training Red Circle

for n = 1:epochs
    
    l0 = red_circle_vect + 0.25 * rand(size(red_circle_vect)) - 0.125;
    expected = [1 0 0];
    
    l1 = feedForward(w0, l0);
    l2 = feedForward(w1, l1);
    
    error_l2 = l2 - expected;

    delta_l2 = error_l2 .* relu_deriv(l2);

    error_l1 = delta_l2 * w1';

    delta_l1 = error_l1 .* relu_deriv(l1);
    
    w1 = w1 - (l1' * delta_l2);
    w0 = w0 - (l0' * delta_l1);
    
end

%% Training Green Circle

for n = 1:epochs
    
    l0 = green_circle_vect + 0.25 * rand(size(green_circle_vect)) - 0.125;
    expected = [0 1 0];
    
    l1 = feedForward(w0, l0);
    l2 = feedForward(w1, l1);
    
    error_l2 = l2 - expected;

    delta_l2 = error_l2 .* relu_deriv(l2);

    error_l1 = delta_l2 * w1';

    delta_l1 = error_l1 .* relu_deriv(l1);
    
    w1 = w1 - (l1' * delta_l2);
    w0 = w0 - (l0' * delta_l1);
    
end

%% Training Blue Circle

for n = 1:epochs
    
    l0 = blue_circle_vect + 0.25 * rand(size(blue_circle_vect)) - 0.125;
    expected = [0 0 1];
    
    l1 = feedForward(w0, l0);
    l2 = feedForward(w1, l1);
    
    error_l2 = l2 - expected;

    delta_l2 = error_l2 .* relu_deriv(l2);

    error_l1 = delta_l2 * w1';

    delta_l1 = error_l1 .* relu_deriv(l1);
    
    w1 = w1 - (l1' * delta_l2);
    w0 = w0 - (l0' * delta_l1);
    
end

%% Evaluating Network

input = red_circle_vect;
out = round(feedForward(w1, feedForward(w0, input)))

input = green_circle_vect;
out = round(feedForward(w1, feedForward(w0, input)))

input = blue_circle_vect;
out = round(feedForward(w1, feedForward(w0, input)))