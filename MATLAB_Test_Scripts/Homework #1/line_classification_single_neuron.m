% Name : line_classification_single_neuron
% Author : Guetz, Erik
% Function: Creates and tests line classification on a neural network 
% with a single perceptron.

clc
clear all
close all

epochs = 10001;

%% Create a line with random slope
slope = 0.5;%rand(1, 1);
int = 0;

x = linspace(0, 1);
line = x*slope + int;

% plot(x, line, 'g'), hold on

points = zeros(3, epochs);

%% Initialize the random points on the cartesian plane
rand_x = rand(1, epochs);
rand_y = rand(1, epochs);

% Classify if a point is above or below the line
% add the points to the array accordingly

for i = 1:epochs

    if rand_y(i) >= (rand_x(i) * slope + int)
       
        points(1, i) = rand_x(i);
        points(2, i) = rand_y(i);
        points(3, i) = 1;
        
    else
        
        points(1, i) = rand_x(i);
        points(2, i) = rand_y(i);
        points(3, i) = 0;
        
    end
    
end

% Initialize the weights for the neuron
w0 = 2 * rand(3, 1) - 1;

fprintf('Training Stage:\n')
tic();

%% Train the perceptron

for i = 1:epochs
    
    inputs = [1, points(1, i), points(2, i)]; % Set the input neurons
    expected = points(3, i); % Expected output

    l1 = feedForward(w0, inputs); % Calculate the hypothesized output

    error_l1 = l1 - expected; % Calculate the output error

    delta_l1 = error_l1 .* relu_deriv(l1); % Use the relu derivative 
                                              % to calculate the layer delta.

    w0 = w0 - (inputs' * delta_l1); % Fix the weights based on the layer delta

end
toc();
% Evaluate the neural network with the already generated points

fprintf('Evaluation Stage:\n')
tic();

acc_array = zeros(1, 100);

for n = 1:100
    
    correct_count = 0;
    
    for i = 1:epochs

       test_input = [1, points(1, i), points(2, i)];
       expected = points(3, i);

       % Calculate the hypothesis
       output = round(feedForward(w0, test_input));

        % Count the number of correct hypotheses 
       if output == expected

           correct_count = correct_count + 1;

       end
       % Plot the points and their respectove colors every 10 epochs
%        if mod(i, 10) == 0
% 
%            if output == 1
% 
%                plot(test_input(2), test_input(3), 'bx')
% 
%            elseif output == 0
% 
%                plot(test_input(2), test_input(3), 'rx')
% 
%            end
% 
%        end

    end
    
    acc_array(n) = correct_count/epochs;

end
%axis([0 1 0 1])

toc();
fprintf('Accuracy: %0.2f\n', (mean(acc_array))*100)