clc
clear vars
close all

shoulder_pos = [0, 0, 0];
elbow_pos = [0, 1, 0];
wrist_pos = [0, 1+sqrt(0.5), sqrt(0.5)];

plot3([shoulder_pos(1), elbow_pos(1), wrist_pos(1)], [shoulder_pos(2), elbow_pos(2), wrist_pos(2)], [shoulder_pos(3), elbow_pos(3), wrist_pos(3)]), grid on

for n=1:100
    
   wrist_pos = wrist_pos + [+0.01, -0.01, 0];
   
   plot3([shoulder_pos(1), elbow_pos(1), wrist_pos(1)], [shoulder_pos(2), elbow_pos(2), wrist_pos(2)], [shoulder_pos(3), elbow_pos(3), wrist_pos(3)]), grid on
   axis([-0.5 0.5 0 2 -1 1])
   pause(0.1);
    
end