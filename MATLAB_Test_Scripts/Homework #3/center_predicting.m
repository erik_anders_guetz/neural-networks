clc
clear
close all

%% Initialize the circle parameters

xdim = 101;
ydim = 101;
radius = 12;

tolerance = 0.1;

%% Initialize Neural Network Parameters

input_dimension = xdim*ydim;
epochs = 1001;
hidden_neurons = 10;
bias_size = 1;
learning_rate = 0.1;

%% Initialize Default Map

map = generate_random_map(xdim, ydim);
num_inputs = 2;

%% Initialize Random Weights

w0 = 2 * rand(input_dimension, hidden_neurons) - 1;
w1 = 2 * rand(hidden_neurons, hidden_neurons) - 1;
w2 = 2 * rand(hidden_neurons, 2) - 1;

%% Training Maps

disp('Training the Neural Network');
tic();
for n = 1:epochs
    
    [input_data, center] = add_random_circle(map);
    input_data = input_data(:)';
    
    l0 = input_data;
    expected = center; % Expected output

    l1 = feedForward(w0, l0);
    l2 = feedForward(w1, l1); % Calculate the hypothesized output
    l3 = feedForward(w2, l2);

    error_l3 = l3 - expected; % Calculate the output error
    
    delta_l3 = error_l3 .* relu_deriv(l3); % Use the sigmoid derivative 
                                           % to calculate the layer delta.

    error_l2 = delta_l3 * w2'; % Calculate the previous layer error given 
                               % the delta from above and current layers weights

    delta_l2 = error_l2 .* relu_deriv(l2); % Use the sigmoid derivative 
                                           % to calculate the layer delta.

    error_l1 = delta_l2 * w1'; % Calculate the previous layer error given 
                               % the delta from above and current layers weights

    delta_l1 = error_l1 .* relu_deriv(l1); % Use the sigmoid derivative 
                                           % to calculate the layer delta.

    w1 = w1 - learning_rate * (l1' * delta_l2); % Fix the weights based on the layer delta
    w0 = w0 - learning_rate * (l0' * delta_l1);
    
end
toc();

%% Evaluating Network
noise_coeff = 2;
disp('Evaluating the Neural Network')
tic();
correct_count = 0;

for n = 1:epochs
    
    [input_data, center] = add_random_circle(map);
    input_data = input_data(:)';

    l0 = input_data;
    expected = center;
    
    % Calculate the hypothesis
    output = feedForward(w2, feedForward(w1, feedForward(w0, l0)));
    % Count the number of correct hypotheses 
    
    if abs(output - expected) <= tolerance

        correct_count = correct_count + 1;

    end

end
toc();
% Print how accurate the evaluated neural network is
fprintf('Accuracy: %0.2f percent\n', (correct_count/(num_inputs*epochs))*100)