function out = relu(x)

    % Returns the Rectified Linear Unit Function output of the input
    % matrix. ReLU function returns x when x > 0, else it returns 0;

    out = x .* (x > 0);
    
end