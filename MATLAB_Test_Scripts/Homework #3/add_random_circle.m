function [ circle_map, center ] = add_random_circle( map )

    radius = size(map, 1) / 8;
    
    center = round(size(map, 1)*(0.5 * rand(1, 2) + 0.25));
    circle_map = zeros(size(map));

    [X, Y] = meshgrid(1:size(map, 1), 1:size(map, 2));
    
    for x = 1:size(map, 1)
        
        for y = 1:size(map, 2)
            
            circle_map(x, y) = rand(1, 1) * ( ((X(x, y)-center(1)).^2 + (Y(x, y)-center(2)).^2 <= radius^2) & ((X(x, y)-center(1)).^2 + (Y(x, y)-center(2)).^2 <= (radius-1)^2) );
            
        end
        
    end
    
    circle_map = circle_map + map;
    
    center = center / size(map, 1);

end

