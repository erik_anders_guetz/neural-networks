/*
 * functions.h
 *
 * Created: 3/19/2018 7:37:29 PM
 *  Author: Erik Guetz
 */ 


#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

void feedForwardLayer1(float weights[3][2], float layer[2][1], float output[3][1]);

#endif /* FUNCTIONS_H_ */