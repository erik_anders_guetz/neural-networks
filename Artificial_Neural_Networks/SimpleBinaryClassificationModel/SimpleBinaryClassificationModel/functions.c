/*
 * functions.c
 *
 * Created: 3/19/2018 7:37:12 PM
 *  Author: Erik Guetz
 */ 

#include "functions.h"

void feedForwardLayer1(float weights[3][2], float layer[2][1], float output[3][1])
{
	int i, j, k;
	
	for(i = 0; i < 3; ++i)
	{
		for(j = 0; j < 2; ++j)
		{
			output[i][j] = 0;
		}
	}
	
	for(i = 0; i < 3; ++i)
	{
		for(j = 0; j < 2; ++j)
		{
			for(k = 0; k < 1; ++k)
			{
				output[i][j] += weights[i][k] * layer[k][j];
			}
		}
	}
}
