/*
 * SimpleBinaryClassificationModel.c
 *
 * Created: 3/19/2018 7:36:43 PM
 * Author : Erik Guetz
 */ 

#include <avr/io.h>
#include <stdio.h>
#include "functions.h"

int main()
{
	float weights[3][2] = {{1, 2}, {3, 4}, {5, 6}};
	float layer[2][1] = {1, 2};
	float output[3][1];
		
	feedForwardLayer1(weights, layer, output);
	
	return 0;
}

